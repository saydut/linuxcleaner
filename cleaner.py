import os
import shutil
import subprocess
import tkinter as tk
from tkinter import messagebox, simpledialog, ttk
from datetime import datetime, timedelta
import requests

CURRENT_VERSION = "1.0.0"  # Mevcut sürüm numarası

def check_for_updates():
    try:
        # GitLab'daki sürüm dosyasının URL'si
        version_info_url = "https://gitlab.com/username/repo/-/raw/main/version.txt"
        
        response = requests.get(version_info_url)
        response.raise_for_status()
        
        new_version_info = response.text.strip()
        new_version, download_url = new_version_info.split('\n')
        
        if new_version > CURRENT_VERSION:
            answer = messagebox.askyesno("Güncelleme Mevcut", f"Yeni sürüm mevcut: {new_version}. Güncellemek ister misiniz?")
            if answer:
                download_and_update(download_url, new_version)
        else:
            messagebox.showinfo("Güncelleme Yok", "Program en güncel sürümde.")
    except Exception as e:
        messagebox.showerror("Hata", f"Güncelleme kontrolü sırasında hata: {e}")

def download_and_update(url, new_version):
    try:
        response = requests.get(url, stream=True)
        response.raise_for_status()

        with open("update_temp.py", "wb") as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)
        
        # Yeni sürüm dosyasını mevcut dosyanın üzerine yazmak
        os.replace("update_temp.py", __file__)
        
        messagebox.showinfo("Güncelleme Tamamlandı", f"Güncelleme başarıyla tamamlandı. Yeni sürüm: {new_version}.")
        
        # Programı yeniden başlat
        subprocess.run(["python3", __file__])
        root.quit()
    except Exception as e:
        messagebox.showerror("Güncelleme Hatası", f"Güncelleme sırasında hata: {e}")

def clear_tmp_dirs(sudo_password):
    tmp_dirs = ['/tmp', '/var/tmp']
    for tmp_dir in tmp_dirs:
        try:
            for item in os.listdir(tmp_dir):
                item_path = os.path.join(tmp_dir, item)
                try:
                    if os.path.isfile(item_path) or os.path.islink(item_path):
                        subprocess.run(['sudo', '-S', 'rm', '-f', item_path], input=sudo_password.encode(), check=True)
                    elif os.path.isdir(item_path):
                        subprocess.run(['sudo', '-S', 'rm', '-rf', item_path], input=sudo_password.encode(), check=True)
                except Exception as e:
                    print(f'{item_path} silinirken hata: {e}')
            print(f'{tmp_dir} temizlendi.')
        except Exception as e:
            print(f'{tmp_dir} temizlenirken hata: {e}')

def clear_user_cache():
    user_home = os.path.expanduser('~')
    cache_dirs = [os.path.join(user_home, '.cache')]
    for cache_dir in cache_dirs:
        try:
            if os.path.isdir(cache_dir):
                shutil.rmtree(cache_dir)
            print(f'{cache_dir} temizlendi.')
        except Exception as e:
            print(f'{cache_dir} temizlenirken hata: {e}')

def optimize_memory(sudo_password):
    try:
        subprocess.run(['sync'], check=True)
        subprocess.run(['sudo', '-S', 'swapoff', '-a'], input=sudo_password.encode(), check=True)
        subprocess.run(['sudo', '-S', 'swapon', '-a'], input=sudo_password.encode(), check=True)
        print('Bellek optimize edildi.')
    except subprocess.CalledProcessError as e:
        print(f'Bellek optimize edilirken hata: {e}')

def clear_old_logs(days, sudo_password):
    log_dir = '/var/log'
    cutoff = datetime.now() - timedelta(days=days)
    for root, dirs, files in os.walk(log_dir):
        for file in files:
            file_path = os.path.join(root, file)
            if os.path.isfile(file_path):
                file_mtime = datetime.fromtimestamp(os.path.getmtime(file_path))
                if file_mtime < cutoff:
                    try:
                        subprocess.run(['sudo', '-S', 'rm', '-f', file_path], input=sudo_password.encode(), check=True)
                        print(f'{file_path} temizlendi.')
                    except Exception as e:
                        print(f'{file_path} silinirken hata: {e}')

def clear_browser_cache():
    user_home = os.path.expanduser('~')
    browser_cache_dirs = [
        os.path.join(user_home, '.cache', 'mozilla'),
        os.path.join(user_home, '.cache', 'google-chrome')
    ]
    for cache_dir in browser_cache_dirs:
        try:
            if os.path.isdir(cache_dir):
                shutil.rmtree(cache_dir)
            print(f'{cache_dir} temizlendi.')
        except Exception as e:
            print(f'{cache_dir} temizlenirken hata: {e}')

def empty_recycle_bin(sudo_password):
    recycle_bin_dir = os.path.expanduser('~/.local/share/Trash/files')
    try:
        if os.path.isdir(recycle_bin_dir):
            subprocess.run(['sudo', '-S', 'rm', '-rf', recycle_bin_dir], input=sudo_password.encode(), check=True)
            os.makedirs(recycle_bin_dir)
        print('Geri dönüşüm kutusu boşaltıldı.')
    except Exception as e:
        print(f'Geri dönüşüm kutusu boşaltılırken hata: {e}')

def clear_downloads_folder():
    downloads_dir = os.path.join(os.path.expanduser('~'), 'Downloads')
    try:
        for item in os.listdir(downloads_dir):
            item_path = os.path.join(downloads_dir, item)
            if os.path.isfile(item_path) or os.path.islink(item_path):
                os.unlink(item_path)
            elif os.path.isdir(item_path):
                shutil.rmtree(item_path)
        print('İndirme klasörü temizlendi.')
    except Exception as e:
        print(f'İndirme klasörü temizlenirken hata: {e}')

def list_startup_programs():
    try:
        result = subprocess.run(['systemctl', 'list-unit-files', '--user', '--state=enabled'], capture_output=True, text=True)
        enabled_units = result.stdout.splitlines()
        return enabled_units
    except subprocess.CalledProcessError as e:
        print(f'Başlangıç programları listelenirken hata: {e}')
        return []

def clear_temp_internet_files(sudo_password):
    temp_internet_dirs = ['/var/tmp', '/tmp']
    for temp_dir in temp_internet_dirs:
        try:
            for item in os.listdir(temp_dir):
                item_path = os.path.join(temp_dir, item)
                try:
                    if os.path.isfile(item_path) or os.path.islink(item_path):
                        subprocess.run(['sudo', '-S', 'rm', '-f', item_path], input=sudo_password.encode(), check=True)
                    elif os.path.isdir(item_path):
                        subprocess.run(['sudo', '-S', 'rm', '-rf', item_path], input=sudo_password.encode(), check=True)
                except Exception as e:
                    print(f'{item_path} silinirken hata: {e}')
            print(f'{temp_dir} temizlendi.')
        except Exception as e:
            print(f'{temp_dir} temizlenirken hata: {e}')

def start_cleanup(tmp, cache, memory, logs, log_days, browser_cache, recycle_bin, downloads, startup_programs, internet_temp_files, sudo_password):
    if tmp:
        clear_tmp_dirs(sudo_password)
    if cache:
        clear_user_cache()
    if memory:
        optimize_memory(sudo_password)
    if logs:
        clear_old_logs(log_days, sudo_password)
    if browser_cache:
        clear_browser_cache()
    if recycle_bin:
        empty_recycle_bin(sudo_password)
    if downloads:
        clear_downloads_folder()
    if startup_programs:
        startup_programs_list = list_startup_programs()
        messagebox.showinfo("Başlangıç Programları", "\n".join(startup_programs_list))
    if internet_temp_files:
        clear_temp_internet_files(sudo_password)
        
    messagebox.showinfo("İşlem Tamamlandı", "Temizlik işlemi tamamlandı.")

def main():
    global root
    root = tk.Tk()
    root.title("Sistem Temizleyici")
    root.geometry("400x380")

    ttk.Label(root, text="Yapmak istediğiniz temizlik işlemlerini seçin:").pack(pady=10)

    tmp_var = tk.BooleanVar()
    cache_var = tk.BooleanVar()
    memory_var = tk.BooleanVar()
    logs_var = tk.BooleanVar()
    browser_cache_var = tk.BooleanVar()
    recycle_bin_var = tk.BooleanVar()
    downloads_var = tk.BooleanVar()
    startup_programs_var = tk.BooleanVar()
    internet_temp_files_var = tk.BooleanVar()
    log_days_var = tk.IntVar()

    ttk.Checkbutton(root, text="/tmp ve /var/tmp dizinlerini temizle", variable=tmp_var).pack(anchor='w')
    ttk.Checkbutton(root, text="Kullanıcı önbelleğini temizle (.cache)", variable=cache_var).pack(anchor='w')
    ttk.Checkbutton(root, text="Belleği optimize et (swap)", variable=memory_var).pack(anchor='w')
    ttk.Checkbutton(root, text="Eski log dosyalarını temizle", variable=logs_var).pack(anchor='w')
    
    ttk.Label(root, text="Log dosyalarını kaç gün önceye kadar temizle:").pack(pady=5)
    log_days_entry = ttk.Entry(root, textvariable=log_days_var)
    log_days_entry.pack(pady=5)

    ttk.Checkbutton(root, text="Tarayıcı önbelleğini temizle", variable=browser_cache_var).pack(anchor='w')
    ttk.Checkbutton(root, text="Geri dönüşüm kutusunu boşalt", variable=recycle_bin_var).pack(anchor='w')
    ttk.Checkbutton(root, text="İndirme klasörünü temizle", variable=downloads_var).pack(anchor='w')
    ttk.Checkbutton(root, text="Başlangıçta çalışan programları listele", variable=startup_programs_var).pack(anchor='w')
    ttk.Checkbutton(root, text="Geçici internet dosyalarını temizle", variable=internet_temp_files_var).pack(anchor='w')
    
    def on_cleanup():
        sudo_password = simpledialog.askstring("Sudo Parolası", "Sudo parolanızı girin:", show='*')
        if not sudo_password:
            messagebox.showerror("Hata", "Sudo parolası gerekli.")
            return
        
        start_cleanup(
            tmp_var.get(),
            cache_var.get(),
            memory_var.get(),
            logs_var.get(),
            log_days_var.get(),
            browser_cache_var.get(),
            recycle_bin_var.get(),
            downloads_var.get(),
            startup_programs_var.get(),
            internet_temp_files_var.get(),
            sudo_password
        )

    button_frame = ttk.Frame(root)
    button_frame.pack(pady=20)

    ttk.Button(button_frame, text="Temizliği Başlat", command=on_cleanup).pack(side=tk.LEFT, padx=10)
    ttk.Button(button_frame, text="Güncellemeleri Kontrol Et", command=check_for_updates).pack(side=tk.LEFT, padx=10)

    root.mainloop()

if __name__ == "__main__":
    main()
